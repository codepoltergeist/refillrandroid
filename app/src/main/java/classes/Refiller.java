package classes;

/**
 * Created by sandeshshetty on 10/11/18.
 */

public class Refiller {

    private String id;
    private String latitude;
    private String longitude;
    private String name;
    private String minpoints;
    private String points;

    public Refiller() {

    }

    public Refiller(String id, String latitude, String longitude, String name, String minpoints, String points) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.minpoints = minpoints;
        this.points = points;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMinpoints() {
        return minpoints;
    }

    public void setMinpoints(String minpoints) {
        this.minpoints = minpoints;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
