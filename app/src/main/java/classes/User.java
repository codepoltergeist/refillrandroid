package classes;

import java.util.ArrayList;

/**
 * Created by sandeshshetty on 10/3/18.
 */

public class User {

    private String displayName;
    private String email;
    private String phoneNumber;
    private String providerId;
    private String uId;
    private ArrayList<Refiller> userPoints;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(User.class)
    }

    public User(String displayName, String email, String phoneNumber, String providerId, String uId) {
        this.displayName = displayName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.providerId = providerId;
        this.uId = uId;
        this.userPoints = new ArrayList<>();
    }

    public ArrayList<Refiller> getUserPoints() {
        return userPoints;
    }

    public void setUserPoints(ArrayList<Refiller> userPoints) {
        this.userPoints = userPoints;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getuId() {
        return uId;
    }

    public void setuId(String uId) {
        this.uId = uId;
    }



}
