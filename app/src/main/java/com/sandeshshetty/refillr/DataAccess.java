package com.sandeshshetty.refillr;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import classes.Refiller;

/**
 * Created by sandeshshetty on 9/26/18.
 * This is a Singelton class
 */

    public class DataAccess {

    private static final DataAccess ourInstance = new DataAccess();
    private FirebaseAuth auth;
    private FirebaseUser currentUser;
    private ArrayList<Refiller> refillerList;

    public static DataAccess getInstance() {
        return ourInstance;
    }

    public DataAccess() {
       // FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }

    /*
    * Auth is used for finding current user logged in and to log out the user
    */
    public FirebaseAuth getAuth() {
        return auth;
    }

    public void setAuth(FirebaseAuth auth) {
        this.auth = auth;
    }


//    Uri photo = user.getPhotoUrl();
//    List<? extends UserInfo> info = user.getProviderData();
//    List<String> providers = user.getProviders();
//    String displayName = user.getDisplayName();
//    String email = user.getEmail();
//    Task<GetTokenResult> token = user.getIdToken(true);
//    FirebaseUserMetadata metaData = user.getMetadata();
//    String phoneNumber = user.getPhoneNumber();
//    String providerId = user.getProviderId();
//    String userid = user.getUid();

    /*
    * Above information can be obtained for currentUser object
    */
    public FirebaseUser getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(FirebaseUser user) {
        this.currentUser = user;
    }

    public ArrayList<Refiller> getRefillerList() {
        return refillerList;
    }

    public void setRefillerList(ArrayList<Refiller> refillerList) {
        this.refillerList = refillerList;
    }

}
