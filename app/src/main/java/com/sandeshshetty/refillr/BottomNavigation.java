package com.sandeshshetty.refillr;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import fragment.MapsFragment;
import fragment.NearbyFragment;

public class BottomNavigation extends AppCompatActivity{

    private TextView mTextMessage;
    private FrameLayout fragment_container;
    private Activity activity;
    private MapsFragment mapsFragment;
    private NearbyFragment nearbyFragment;

    /**
    * Listeners created for bottom navigation at the main page
     * This is triggered everytime bottom navigation item is clicked
     */
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    //mTextMessage.setText(R.string.title_home);
                    return true;
                case R.id.navigation_dashboard:
                    mapsFragment = new MapsFragment();
                    mapsFragment.setContext(BottomNavigation.this, activity);
                    setUpFragments(mapsFragment);

                    return true;
                case R.id.navigation_notifications:
                    nearbyFragment = new NearbyFragment();
                    nearbyFragment.setContext(BottomNavigation.this, activity);
                    setUpFragments(nearbyFragment);
                    return true;
                case R.id.navigation_refill:
                 //   mTextMessage.setText(R.string.title_notifications);
                    return true;
//                case R.id.navigation_refill1:
//                 //   mTextMessage.setText(R.string.title_notifications);
//                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        activity = this;

        fragment_container = (FrameLayout)findViewById(R.id.fragment_container);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_dashboard);

//        String displayName = DataAccess.getInstance().getCurrentUser().getDisplayName();

//        Snackbar.make(fragment_container,"Welcome "+displayName,Snackbar.LENGTH_LONG).show();



    }

    /**
     * Setups the the required fragments when an item at the bottom is clicked
     * @param fragment the fragment which needs to be attached at the view
     */
    private void setUpFragments(Fragment fragment) {
        FragmentManager manager = getFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.fragment_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
//        List<android.support.v4.app.Fragment> fragments = getSupportFragmentManager().getFragments();
//        if (fragments != null) {
//            for (android.support.v4.app.Fragment fragment : fragments) {
//                fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
//            }
//        }
        if(mapsFragment != null)
        mapsFragment.onRequestPermissionsResult(requestCode,permissions,grantResults);

    }

}
