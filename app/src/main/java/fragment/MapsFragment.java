package fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.support.v7.app.AlertDialog;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCanceledListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sandeshshetty.refillr.DataAccess;
import com.sandeshshetty.refillr.R;
import com.sandeshshetty.refillr.Utility;

import java.util.ArrayList;

import classes.Refiller;

public class MapsFragment extends Fragment {


    private GoogleMap mMap;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private FusedLocationProviderClient fusedLocationProviderClient;
    private Location mLastKnownLocation;
    private boolean locationPermissionGranted;
    private View root;
    private MapFragment mapFragment;
    private Context context;
    private Activity activity;
    private ArrayList<Refiller> refillerList;
    private BottomSheetBehavior bottomSheetBehavior;
    private RecyclerView recyclerViewLocation;
    private MapAdapter mapAdapter;

    public MapsFragment() {

    }

    public void setContext(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;

    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {

        root = layoutInflater.inflate(R.layout.activity_maps, viewGroup, false);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(context);
        refillerList = new ArrayList<>();

        if (mapFragment == null) {
            mapFragment = MapFragment.newInstance();
            // mapFragment.getMapAsync(callback);
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    getMapReady(googleMap);
                }
            });
        }

        // R.id.map is a FrameLayout, not a Fragment
        getChildFragmentManager().beginTransaction().replace(R.id.map, mapFragment).commit();


        //Code for persistent bottom sheet
        View bottomSheet = root.findViewById(R.id.bottom_sheet);
        recyclerViewLocation = root.findViewById(R.id.recylerMaps);
        bottomSheetBehavior= BottomSheetBehavior.from(bottomSheet);
        bottomSheetBehavior.setState(BottomSheetBehavior.PEEK_HEIGHT_AUTO);

        mapAdapter = new MapAdapter(refillerList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewLocation.setLayoutManager(mLayoutManager);
        recyclerViewLocation.setItemAnimator(new DefaultItemAnimator());
        recyclerViewLocation.setAdapter(mapAdapter);

        //MapModalSheet.newInstance(30).show(getFragmentManager(), "dialog");

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        //Toast.makeText(activity,"MapONStart",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStop() {
        //Toast.makeText(activity,"MapONStop",Toast.LENGTH_LONG).show();
        if(refillerList != null) {
            DataAccess.getInstance().setRefillerList(refillerList);
        }
        super.onStop();
    }

    /**
    * Fetch data from database for refillers and display on map
    */
    private void fetchDataForRefillers(){
        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference();

        reference.child("refill").child("water").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot value : dataSnapshot.getChildren()){
                    Refiller refiller = value.getValue(Refiller.class);
                    double lat = Double.parseDouble(refiller.getLatitude());
                    double longi = Double.parseDouble(refiller.getLongitude());
                    LatLng latLng = new LatLng(lat,longi);
                    String name = refiller.getName();
                    refillerList.add(refiller);
                    addRefillerMaker(name,latLng,R.drawable.ic_waterrefill, R.color.waterrefill);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        reference.child("refill").child("coffee").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot value : dataSnapshot.getChildren()){
                    Refiller refiller = value.getValue(Refiller.class);
                    double lat = Double.parseDouble(refiller.getLatitude());
                    double longi = Double.parseDouble(refiller.getLongitude());
                    LatLng latLng = new LatLng(lat,longi);
                    String name = refiller.getName();
                    refillerList.add(refiller);
                    mapAdapter.notifyDataSetChanged();
                    addRefillerMaker(name,latLng,R.drawable.ic_cafe,R.color.colorCoffee);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    /**
     * Adds markers on the map with icon and on the point
     * @param name of the place
     * @param latLng latitude and longitude of the marker
     * @param icon marker icon to be placed on the map
     * @param color icon color
     */
    private void addRefillerMaker(String name, LatLng latLng, int icon, int color) {
        BitmapDescriptor markerIcon = Utility.vectorToBitmap(icon,
                ContextCompat.getColor(context,
                       color),context);
        mMap.addMarker(new MarkerOptions().title(name).icon(markerIcon).position(latLng));
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    public void getMapReady(GoogleMap googleMap) {
        mMap = googleMap;

//        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID); // this makes the map type with tress and roads
        mMap.setIndoorEnabled(true);

        //getCurrentLocation();
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        getLocationPermision();
        fetchDataForRefillers();


//        LatLng sydney = new LatLng(-36.912051, 174.749005);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 19));
    }

    /**
     * A dialog to ask user for enabling gps
     */
    public void enableGPS() {

         new AlertDialog.Builder(context)
                 .setTitle("To continue, turn on device location, which uses Google's location")
                 .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),1);
                    }
                    })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                 .setCancelable(false)
                 .show();
    }


    /*
    * Request location permssion so that we can get the location of the device
     * The request permission is obtained in the call back part
    */
    private void getLocationPermision() {

        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true;
            if(locationPermissionGranted && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                getDeviceLocation();
            } else {
                enableGPS();
            }
        } else {

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }


    }

    /*
    * Update the map UI showing the current UI icon
    * */
    private void updateMapUi() {

        if (mMap == null) {

            return;
        }

        try {

            if (locationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
//                getDeviceLocation();
            } else {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermision();

            }

        } catch (SecurityException e) {

        }

    }

    /*
    * Gets the most recent location of the device obtained
    */
    private void getDeviceLocation() {

        try {
            updateMapUi();
            if (locationPermissionGranted) {
                Task<Location> locationResult = fusedLocationProviderClient.getLastLocation();



                locationResult.addOnCompleteListener(activity, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();

                            addMarker(mLastKnownLocation);
                        } else {

                            Toast.makeText(activity, "not found", Toast.LENGTH_LONG).show();
                        }
                        if(mLastKnownLocation == null)
                            getCurrentLocation();

                    }
                });
                locationResult.addOnFailureListener(activity, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(activity,"failed",Toast.LENGTH_LONG).show();
                    }
                });
                locationResult.addOnCanceledListener(activity, new OnCanceledListener() {
                    @Override
                    public void onCanceled() {
                        Toast.makeText(activity,"canceled",Toast.LENGTH_LONG).show();

                    }
                });
            }

        } catch (SecurityException e) {

        }

    }

    /*
    * Adds a marker at the provided location
    */
    public void addMarker(Location location) {
        if (location != null) {
            LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
            mMap.addMarker(new MarkerOptions().position(currentLocation).title("ME"));
//            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 19));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(currentLocation)      // Sets the center of the map to Mountain View
                    .zoom(17)                   // Sets the zoom
                    .bearing(0)                // Sets the orientation of the camera to east
                    .tilt(50)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
    }

    public void getCurrentLocation() {

//        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {

            @Override
            public void onLocationChanged(Location location) {
                addMarker(location);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        } else {
            // we already have permissions
//            mMap.setMyLocationEnabled(true);
//            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            locationPermissionGranted = true;
            if(locationPermissionGranted && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                getDeviceLocation();
            } else {
                enableGPS();
            }
//            updateMapUi();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

            switch (requestCode) {
                case 1:
                    if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                       getDeviceLocation();
                    }else {
                        getLocationPermision();
                    }
                    break;
            }


    }
}
