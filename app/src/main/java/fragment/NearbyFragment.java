package fragment;


import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcode;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetector;
import com.google.firebase.ml.vision.barcode.FirebaseVisionBarcodeDetectorOptions;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.sandeshshetty.refillr.DataAccess;
import com.sandeshshetty.refillr.R;
import com.wonderkiln.camerakit.CameraKitError;
import com.wonderkiln.camerakit.CameraKitEvent;
import com.wonderkiln.camerakit.CameraKitEventListener;
import com.wonderkiln.camerakit.CameraKitImage;
import com.wonderkiln.camerakit.CameraKitVideo;
import com.wonderkiln.camerakit.CameraView;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import classes.Refiller;
import classes.User;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 *
 * to handle interaction events.
 * Use the {@link NearbyFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class NearbyFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String currentUserId;

//    private OnFragmentInteractionListener mListener;

    private View root;

    private Message message;
    private MessageListener messageListener;
    private Context context;
    private Activity activity;

    private Button btPublish;
    private Button btSubscribe;
    private Button btn_detect;

    CameraView cameraView;

    private DatabaseReference reference;

    public NearbyFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NearbyFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NearbyFragment newInstance(String param1, String param2) {
        NearbyFragment fragment = new NearbyFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public void setContext(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        root =  inflater.inflate(R.layout.qrcodescanner, container, false);

        btn_detect = root.findViewById(R.id.btn_detect);
        cameraView = root.findViewById(R.id.cameraView);

        reference = FirebaseDatabase.getInstance().getReference();

        if(DataAccess.getInstance() != null)
        currentUserId = DataAccess.getInstance().getCurrentUser() != null ? DataAccess.getInstance().getCurrentUser().getUid():"";

        btn_detect.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                cameraView.start();
                cameraView.captureImage();
            }
        });

        cameraView.addCameraKitListener(new CameraKitEventListener() {
            @Override
            public void onEvent(CameraKitEvent cameraKitEvent) {

            }

            @Override
            public void onError(CameraKitError cameraKitError) {

            }

            @Override
            public void onImage(CameraKitImage cameraKitImage) {
                Bitmap bitmap = cameraKitImage.getBitmap();
                bitmap = Bitmap.createScaledBitmap(bitmap, cameraView.getWidth(),cameraView.getHeight(),false);
                cameraView.stop();
                runDetector(bitmap);
            }



            @Override
            public void onVideo(CameraKitVideo cameraKitVideo) {

            }
        });


//        root =  inflater.inflate(R.layout.fragment_nearby, container, false);
        //subscribeListener();

        return root;
    }

    private void runDetector(Bitmap bitmap) {
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(bitmap);
        FirebaseVisionBarcodeDetectorOptions options = new FirebaseVisionBarcodeDetectorOptions.Builder()
                .setBarcodeFormats(
                        FirebaseVisionBarcode.FORMAT_QR_CODE,
                        FirebaseVisionBarcode.FORMAT_PDF417
                )
                .build();
        FirebaseVisionBarcodeDetector detector = FirebaseVision.getInstance().getVisionBarcodeDetector(options);

        detector.detectInImage(image)
                .addOnSuccessListener(new OnSuccessListener<List<FirebaseVisionBarcode>>() {
                    @Override
                    public void onSuccess(List<FirebaseVisionBarcode> firebaseVisionBarcodes) {
                        processResult(firebaseVisionBarcodes);
                    }
                })
        .addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(activity, e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

    }

    private void processResult(List<FirebaseVisionBarcode> firebaseVisionBarcodes) {
        Toast.makeText(activity,"Test3:"+firebaseVisionBarcodes.size(),Toast.LENGTH_LONG).show();
        for (FirebaseVisionBarcode item : firebaseVisionBarcodes) {
            int value_type = item.getValueType();
            Toast.makeText(activity,"Test1:"+item.getValueType(),Toast.LENGTH_LONG).show();
            switch (value_type){
                case FirebaseVisionBarcode.TYPE_TEXT:
                {
                    Log.d("Barcode",item.getRawValue());
                    //Toast.makeText(activity,"Test:"+item.getRawValue(),Toast.LENGTH_LONG).show();
                    updateUserPoints(item.getRawValue());

                }
                break;
                case FirebaseVisionBarcode.TYPE_URL:
                {

                }
                default:
                    break;
            }
        }
    }

    private void updateUserPoints(String rawValue){
        if(rawValue.contains(";")) {
            String id = rawValue.split(";")[0];
            String name = rawValue.split(";")[1];

            ArrayList<Refiller> refillers = DataAccess.getInstance().getRefillerList();

            for(Refiller refiller: refillers) {
                String presentId = refiller.getId();
                if(presentId.equals(id.trim())){
//                    Toast.makeText(activity,name,Toast.LENGTH_LONG).show();
                    findUser(currentUserId,presentId,refiller);
                }
            }

        }


    }

    private void findUser(final String uid, final String cafeId, @NonNull final Refiller refiller) {
        reference.child("user").child(uid).child("userPoints").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue() == null) {
                    Toast.makeText(activity,"Refiller not present"+cafeId,Toast.LENGTH_LONG).show();
                    addNewRefillerToUser(uid,cafeId,refiller,null);
                }else if(dataSnapshot.hasChild(cafeId)) {
                    for(DataSnapshot value: dataSnapshot.getChildren()){
                        Refiller prevrefiller = value.getValue(Refiller.class);
                        if(prevrefiller.getId().equalsIgnoreCase(cafeId)) {
                            updateUserRefiller(uid,cafeId,refiller,prevrefiller);
                            break;
                        }
                    }

                }else {
                    Toast.makeText(activity,"Does not have refiller",Toast.LENGTH_LONG).show();
                    addNewRefillerToUser(uid,cafeId,refiller,null);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(context,databaseError.getMessage(),Toast.LENGTH_LONG).show();
            }
        });


    }

    public void addNewRefillerToUser(String uid,String cafeId, Refiller refiller,Refiller pRefill) {
        Refiller refill=null;
        if(pRefill == null) {
            refill = new Refiller(refiller.getId(),refiller.getLatitude(),refiller.getLongitude(),refiller.getName(),refiller.getMinpoints(),refiller.getPoints());
        }else {
            return;
        }

        reference.child("user").child(uid).child("userPoints").child(cafeId).setValue(refill).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(activity,"Successfully added cafe point",Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(activity,"Failed:"+e.getMessage(),Toast.LENGTH_LONG).show();

            }
        });
    }

    private void updateUserRefiller(String uid,String cafeId,Refiller refiller,Refiller pRefill) {
        int totalPoints = Integer.parseInt(refiller.getMinpoints()) + Integer.parseInt(pRefill.getMinpoints());

        reference.child("user").child(uid).child("userPoints").child(cafeId).child("minpoints").setValue(String.valueOf(totalPoints)).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(activity, "Successfully updated Points of user", Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(activity, "Failed updating Points of user", Toast.LENGTH_LONG).show();

            }
        });



    }


    //Nearby Message methods
    private void publish(String msg) {
        Log.i("publish", "Publishing message: " + message);
        message = new Message(msg.getBytes());
        Nearby.getMessagesClient(context).publish(message);
    }

    // Subscribe to receive messages.
    private void subscribe() {
//        Log.i(TAG, "Subscribing.");
        Nearby.getMessagesClient(context).subscribe(messageListener);//, options);
    }

    private void subscribeListener() {
        messageListener = new MessageListener() {
            @Override
            public void onFound(Message message) {
                Log.d("NearbyFound", "Found message: " + new String(message.getContent()));
                // Toast.makeText(activity,"Found message:"+ new String(message.getContent()),Toast.LENGTH_LONG).show();
                String uid = new String(message.getContent());
                //findUser(uid);
            }

            @Override
            public void onLost(Message message) {
                Log.d("NearbyLost", "lost sight of message: " + new String(message.getContent()));
                Toast.makeText(activity,"Found message: ",Toast.LENGTH_LONG).show();
            }
        };

        btPublish = root.findViewById(R.id.btPublish);
        btPublish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(DataAccess.getInstance().getCurrentUser() != null) {
                    String uid = DataAccess.getInstance().getCurrentUser().getUid();
                    publish(""+uid);
                }else{
                    Toast.makeText(context,"null",Toast.LENGTH_LONG).show();
                }
            }
        });

        btSubscribe = root.findViewById(R.id.btSubscribe);
        btSubscribe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Toast.makeText(context,"subs",Toast.LENGTH_LONG).show();
                subscribe();
            }
        });

    }

// end of nearby message methods


    @Override
    public void onStart() {
        super.onStart();
        cameraView.start();
//        Nearby.getMessagesClient(context).publish(message);
//        Nearby.getMessagesClient(context).subscribe(messageListener);
    }

    @Override
    public void onStop() {
        cameraView.stop();
//        if (message != null)
//        Nearby.getMessagesClient(context).unpublish(message);
//        Nearby.getMessagesClient(context).unsubscribe(messageListener);
        super.onStop();
    }

    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
//    }
//
//    @Override
//    public void onAttach(Context context) {
//        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
//    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
//    public interface OnFragmentInteractionListener {
//        // TODO: Update argument type and name
//        void onFragmentInteraction(Uri uri);
//    }
}
